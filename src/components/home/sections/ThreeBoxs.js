import React, { useState, useEffect } from 'react';
import { getPostsByType } from "../../../utils/Models"

export default function Products() {

    const [posts, setPosts] = useState([]);

    useEffect(async () => {


        getPostsByType(13).then((res) => {
            const res_data = res.data;
            const posts = [];


            for (let index in res_data.data) {
                let row = res_data.data[index];

                posts.push({
                    title: row.title,
                    metas: row.metas
                })
            }

            setPosts(posts);

        })

    }, [])

    return (
        <section id="three_boxs">
            <div className="container-fluid">
                <div className="row">
                    
                    {
                        posts.map(item => {
                            return (
                                <div className="col-lg-4">
                                    <div className="company-characteristics">
                                        <div className="border">
                                            <i className={item.metas.icon_class}></i>
                                            <h4><a href={(item.metas.link) ? item.metas.link : '#' }>{item.title}</a></h4>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                    
                </div>


            </div>
        </section>
    )
}