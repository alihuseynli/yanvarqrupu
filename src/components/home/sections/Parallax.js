import React, { useState, useEffect } from 'react';
import { getPostById } from "../../../utils/Models"

export default function Parallax() {
    const [post, setPost] = useState([]);
    const [metas, setMetas] = useState([]);

    useEffect(async () => {

        getPostById(6).then((res) => {
            const res_data = res.data;

            setPost(res_data);
            setMetas(res_data.metas);

        })

    }, [])


    return (
        <section id="parallax" style={{ backgroundImage : "url("+post.featured+")" }} >
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="bg-parallax">
                            <h2>{post.title}</h2>
                            <p>{post.excerpt}</p>
                            <a
                                href={metas.link}
                                target="_blank"
                            ><span>view presentattion</span></a
                            >
                            <div className="bottom-image"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}