export default function Counter() {
    return (
        <section id="blog">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-4 col-sm-6 col-md-4">
                        <div className="blog-item">
                            <div className="blog-header">
                                <img src="assets/images/user-4-49x49.jpg" alt="" />
                                <a href="singleBlog.html"><span>by</span> Ann Smith</a>
                                <p>May 17, 2018</p>
                            </div>
                            <div className="blog-body">
                                <a href="singleBlog.html">
                                    <img src="assets/images/post-16-370x267.jpg" alt=""
                                    /></a>
                            </div>
                            <div className="blog-footer">
                                <a href="singleBlog.html"
                                >Why Organic Farming Keeps Getting Popular Globally</a
                                >
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-sm-6 col-md-4">
                        <div className="blog-item">
                            <div className="blog-header">
                                <img src="assets/images/user-6-49x49.jpg" alt="" />
                                <a href="singleBlog.html"><span>by</span> Kate William</a>
                                <p>May 17, 2018</p>
                            </div>
                            <div className="blog-body">
                                <a href="singleBlog.html"
                                ><img src="assets/images/post-17-370x267.jpg" alt=""
                                    /></a>
                            </div>
                            <div className="blog-footer">
                                <a href="singleBlog.html"
                                >Everyday Dinner Choices for a Healthier, Happier You</a
                                >
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-sm-6 col-md-4">
                        <div className="blog-item">
                            <div className="blog-header">
                                <img src="assets/images/user-5-49x49.jpg" alt="" />
                                <a href="singleBlog.html"><span>by</span> Peter Milan</a>
                                <p>May 17, 2018</p>
                            </div>
                            <div className="blog-body">
                                <a href="singleBlog.html">
                                    <img src="assets/images/post-18-370x267.jpg" alt=""
                                    /></a>
                            </div>
                            <div className="blog-footer">
                                <a href="singleBlog.html">Standardizing the Organic Industry in the USA</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}