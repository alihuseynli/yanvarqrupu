//import React, { useState, useEffect } from 'react';

import Slider from "./sections/Slider"
import Parallax from "./sections/Parallax"
import ThreeBoxs from "./sections/ThreeBoxs"
import Products from "./sections/Products"
import Counter from "./sections/Counter"
import Blog from "./sections/Blog"
import Partners from "./sections/Partners"


export default function Index() {

    return (
        <>
            <Slider />
            <ThreeBoxs />
            <Products type="first" />
            <Parallax />
            <Products type="second" />
            <Counter />
            <Partners />
            <Blog />
        </>
    );
}