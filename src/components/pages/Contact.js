import React, { useState, useEffect } from 'react';
import { TranslateList} from "../../App"

import { AppendStyle, AppendScript } from "../../utils/Append"
import PageTop from "../template/PageTop"

import { sendFormData, getPostById } from "../../utils/Models"

export default (props) => {
    const [first_name, setFirst_name] = useState('')
    const [last_name, setLast_name] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [message, setMessage] = useState('')
    const [showError, setShowError] = useState(false)
    const [mapData, setMapData] = useState({});

    const tr_list = React.useContext(TranslateList);


    useEffect(() => {
        AppendScript(true, 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD7TypZFTl4Z3gVtikNOdGSfNTpnmq-ahQ&callback=initMap&language=az');

        AppendStyle('contact');

        getPostById(7).then((res) => {
            setMapData(res.data.metas.map)
        })


        AppendScript('true', 'assets/js/map.js');


    }, []);

    const sendMessage = (e) => {
        e.preventDefault();

        if (first_name && last_name && email && phone && message) {
            setShowError(false)

            sendFormData(
                {
                    contact_form : true,
                    title: first_name+' '+last_name,
                    'meta[1]': email,
                    'meta[2]': phone,
                    content: message
                }
            ).then((res) => {
                
                setFirst_name('');
                setLast_name('');
                setEmail('');
                setPhone('');
                setMessage('');

                document.getElementById('contact_form').reset();

                alert("Mesajiniz ugurla gonderildi")
                
            })
            

        }else{
            setShowError(true)
        }
    }

    const setState = (function_name, e) => {

        function_name(e.target.value);
    }

    

    return (
        <>
            <PageTop id="7" />

            <section id="info">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-sm-6 col-md-4">
                            <div className="info-detail">
                                <i className="fas fa-mobile-alt"></i>
                                <p>+1 323-913-4688</p>
                                <p>+1 323-888-4554</p>
                            </div>
                        </div>
                        <div className="col-lg-4 col-sm-6 col-md-4">
                            <div className="info-detail">
                                <i className="fas fa-map-marker-alt"></i>
                                <p>4730 Crystal Springs Dr, Los Angeles, CA 90027</p>
                            </div>
                        </div>
                        <div className="col-lg-4 col-sm-6 col-md-4">
                            <div className="info-detail">
                                <i className="far fa-envelope-open"></i>
                                <p>mail@demolink.org</p>
                                <p>info@demolink.org</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="contactForm">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6  col-md-6">
                            <div className="contact-map">
                                {/* <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d97267.1134908569!2d49.9515392!3d40.3734528!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1616403724772!5m2!1sen!2s"
                                    width="600"
                                    height="450"
                                    style={{"border": "0"}}
                                    allowfullscreen=""
                                    loading="lazy"
                                ></iframe> */}
                                <div id="map" className="map_box" data-zoom={mapData.zoom} data-latitude={mapData.lat} data-longitude={mapData.lon} data-icon="assets/images/map_marker.png" data-title="Baki" ></div>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6">
                            <div className="contact-form">
                                <h5>Contact us</h5>
                                <form onSubmit={sendMessage.bind(this)} id="contact_form">
                                    <input type="text" className="fullname" placeholder={tr_list.adiniz}  onKeyUp={setState.bind(this, setFirst_name)}  />
                                    {
                                        (showError && !first_name) && <span>{tr_list.error_msg}</span>
                                    }
                                    <input type="text" className="fullname" placeholder={tr_list.soyadiniz}  onKeyUp={setState.bind(this, setLast_name)} />
                                    <input type="email" className="email" placeholder={tr_list.email} onKeyUp={setState.bind(this, setEmail)}  />
                                    <input type="tel" className="email" placeholder={tr_list.tel} onKeyUp={setState.bind(this, setPhone)} />
                                    <textarea name="" id="" cols="25" rows="10" placeholder={tr_list.mesajiniz}  onKeyUp={setState.bind(this, setMessage)} ></textarea>
                                    <button type="submit" className="submit_btn" >
                                        <span>{tr_list.gonder}</span>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}
