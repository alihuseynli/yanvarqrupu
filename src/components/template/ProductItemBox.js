export default (props) => {
    const {item} = props;
    
    return (
        <div class="col-lg-4 col-sm-6">
            <div class="product-item-image">
                <img src={item.featured} alt={item.title} />
                <div class="product-overlay">
                    <a href="#"><span>add to cart</span></a>
                </div>
            </div>
            <div class="product-detail">
                <h6><a href={"/shop_item/" + item.link}>{item.title}</a></h6>
                {
                    item.has_discount ? <>
                        <p class="old-price">${item.price}</p>
                        <p class="new-price">${item.discount_price}</p>
                    </> : <p class="new-price">${item.price}</p>
                }
            </div>
        </div>
    )
}