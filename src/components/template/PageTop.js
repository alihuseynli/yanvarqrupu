import React, { useState, useEffect } from 'react';

import { getPostById } from "../../utils/Models"

export default function PageTop(props) {
    const [title, setTitle] = useState('')
    const [coverImage, setCoverImage] = useState('')

    useEffect(async () => {

        getPostById(props.id).then((res) => {
            let data=res.data;

            setTitle(data.title);
            setCoverImage(data.featured);
        })

    }, [])

    return (
        <section id="about">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="about-title" style={{ backgroundImage: "url(" + coverImage+")" }} >
                            <h2>{title}</h2>
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li class="light-color">{title}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}