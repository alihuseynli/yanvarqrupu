export default function NewsItem(props) {
    const { title, desc, author} = props.data;

    return (
        <li>
            {/* <h3>{props.title}</h3>
            <p>{props.desc}</p> */}

            <h3>{title}</h3>
            <span>{author}</span>
            <p>{desc}</p>
        </li>
    );
}
