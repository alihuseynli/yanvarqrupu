import React, { useState, useEffect } from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom'

import { getTranslate} from "./utils/Models"

import Header from "./components/template/Header"
import Footer from "./components/template/Footer"
import Index from "./components/home/Index"

//pages
import About from "./components/pages/About"
import News from "./components/pages/News"
import Contact from "./components/pages/Contact"
import Shop from "./components/pages/Shop"
import Cart from "./components/pages/Cart"

//post
import NewsSingle from "./components/posts/News"
import ProductSingle from "./components/posts/ProductSingle"

export const TranslateList = React.createContext();


function App() {
  const [translate, setTranslate] = useState({});

  useEffect(() => {

    getTranslate().then((res) => {

      setTranslate(res.data);
    })

  }, [])


  return (
    <>
      <TranslateList.Provider value={translate}>
        <BrowserRouter>
          <Header />

          <main>
            <Switch>
              {/* <Route path="/" exact component={Index} /> */}
              <Route path="/" exact={true} >
                <Index />
              </Route>

              <Route path="/about" exact component={About} />
              <Route path="/blog" component={News} />
              <Route path="/read_news/:news_link" exact component={NewsSingle} />
              <Route path="/contact" exact component={Contact} />
              <Route path="/shop" component={Shop} />
              <Route path="/shop_item/:item_link" exact component={ProductSingle} />
              <Route path="/cart" exact component={Cart} />

            </Switch>

          </main>

          <Footer />

        </BrowserRouter>

      </TranslateList.Provider>
    </>
  );
}

export default App;
